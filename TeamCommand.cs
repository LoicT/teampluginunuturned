﻿using System;
using Cysharp.Threading.Tasks;
using OpenMod.API.Commands;
using OpenMod.API.Users;
using OpenMod.Core.Commands;
using OpenMod.Core.Users;
using OpenMod.Unturned.Commands;
using OpenMod.Unturned.Players;
using OpenMod.Unturned.Users;
using SDG.Unturned;

namespace TeamGrangePlugin
{
    [Command("team")]
    [CommandDescription("Join a team")]
    [CommandSyntax("<team name/number>")]
    [CommandActor(typeof(UnturnedUser))]
    public class CommandTeam : UnturnedCommand
    {
        private TeamPlugin teamPlugin;

        public CommandTeam(TeamPlugin teamPlugin, IServiceProvider serviceProvider) : base(serviceProvider)
        {
            this.teamPlugin = teamPlugin;
        }


        protected override async UniTask OnExecuteAsync()
        {
            if(Context.Parameters.Length != 1)
            {
                throw new UserFriendlyException("Should only have 1 parameters");
            }
            UnturnedUser? unturnedUser = Context.Actor as UnturnedUser;
            if (unturnedUser == null)
            {
                throw new UserFriendlyException("Should only be send by a player");
            }

            await Context.Actor.PrintMessageAsync($"Adding ${unturnedUser.Id} to team ${Context.Parameters[0]}");
            this.AddPlayerToTeam(Context.Parameters[0], unturnedUser);
        }

        protected void AddPlayerToTeam(String teamName, UnturnedUser player)
        {
            this.teamPlugin.storage.AddPlayerToTeam(teamName, player);
        }
    }
}
