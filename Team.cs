﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using Cysharp.Threading.Tasks;
using OpenMod.Unturned.Users;

namespace TeamGrangePlugin
{

    public class Team
    {
        public List<UnturnedUser> players = new List<UnturnedUser>();
        public String name;

        public Team(String name)
        {
            this.name = name;
        }

        public void RemovePlayer(UnturnedUser player)
        {
            this.players.Remove(player);
        }

        public async UniTask StartGame(Vector3 position)
        {
            foreach (UnturnedUser user in players)
            {
                await user.PrintMessageAsync("Teleporting to team...");
                await user.Player.SetPositionAsync(position);
                await user.Player.SetHealthAsync(user.Player.MaxHealth);
                await user.Player.SetHungerAsync(user.Player.MaxHunger);
            }
        }

    }
}
