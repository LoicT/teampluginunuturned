﻿using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using OpenMod.Core.Commands;
using OpenMod.Unturned.Commands;
using OpenMod.Unturned.Users;
using SDG.Unturned;

namespace TeamGrangePlugin
{
    [Command("resetteam")]
    [CommandDescription("Reset all teams")]
    [CommandSyntax("")]
    public class ResetTeam : UnturnedCommand
    {
        private TeamPlugin teamPlugin;

        public ResetTeam(TeamPlugin teamPlugin, IServiceProvider serviceProvider) : base(serviceProvider)
        {
            this.teamPlugin = teamPlugin;
        }

        protected override async UniTask OnExecuteAsync()
        {
            this.teamPlugin.storage.teams = new List<Team>();
        }
    }
}
