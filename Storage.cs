﻿using OpenMod.Unturned.Users;
using SDG.Unturned;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeamGrangePlugin
{
    public class Storage
    {
        public List<Team> teams = new List<Team>();

        /**
         * Add a player to the team given 
         * Create the team if it does not exist
         */
        public void AddPlayerToTeam(String teamName, UnturnedUser player)
        {
            this.teams.ForEach(x => x.RemovePlayer(player));
            Team team = teams.Find(x => x.name.Equals(teamName));
            if(team == null)
            {
                team = new Team(teamName);
                this.teams.Add(team);
            }
            team.players.Add(player);
        }

        public List<UnturnedUser> GetAllPlayers()
        {
            return this.teams.Aggregate(new List<UnturnedUser>(), 
                (List<UnturnedUser> acc, Team team) =>
                {
                    team.players.ForEach(x => acc.Add(x));
                    return acc;
                }
            );
        }
    }
}
