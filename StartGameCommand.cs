﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using Cysharp.Threading.Tasks;
using MoreLinq;
using OpenMod.Core.Commands;
using OpenMod.Unturned.Commands;
using OpenMod.Unturned.Users;
using SDG.Unturned;

namespace TeamGrangePlugin
{
    [Command("startgame")]
    [CommandDescription("Start the game")]
    [CommandSyntax("")]
    public class StartGameCommand : UnturnedCommand
    {
        private TeamPlugin teamPlugin;

        public StartGameCommand(TeamPlugin teamPlugin, IServiceProvider serviceProvider) : base(serviceProvider)
        {
            this.teamPlugin = teamPlugin;
        }


        protected override async UniTask OnExecuteAsync()
        {
            await Context.Actor.PrintMessageAsync("Start game");
            List<Vector3> positions = this.getFurtherPositions();
            for(int i=0; i< this.teamPlugin.storage.teams.Count; i++)
            {
                Team team = this.teamPlugin.storage.teams[i];
                await Context.Actor.PrintMessageAsync($"Starting game for team {team.name}");
                await team.StartGame(positions[i]);
            }
        }

        /**
         * Get a list of point equals to the number of team.
         * To find the furthest point we will use the Furthest Point Sampling between all the players position.
         */
        private List<Vector3> getFurtherPositions()
        {
            //Remaining points
            List<Vector3> remainings = new List<Vector3>(this.teamPlugin.storage.GetAllPlayers().Select(x => x.Player.Transform.Position));
            //FartherPoints
            List<Vector3> farthest = new List<Vector3>();

            Vector3 first = this.getFirstPoint();
            remainings.Remove(first);
            farthest.Add(first);

            // Find the amount of point needed
            while(farthest.Count < this.teamPlugin.storage.teams.Count)
            {
                // Should not happen but here as a security
                if (remainings.Count == 0)
                {
                    throw new Exception("not enough player in teams");
                }
                Vector3 farther = Vector3.Zero;
                float maxDistance = 0;

                farthest.ForEach((Vector3 far) =>
                {
                    // Find the closest point of all the remainings points
                    Vector3 closest = Vector3.Zero;
                    float minDistance = float.MaxValue;
                    remainings.ForEach((Vector3 remain) =>
                    {
                        float distance = Vector3.Distance(far, remain);
                        if (minDistance > distance)
                        {
                            minDistance = distance;
                            closest = remain;
                        }
                    });
                    // If it is farther than the previous point replace
                    if(maxDistance< minDistance)
                    {
                        farther = closest;
                        maxDistance = minDistance;
                    }
                });
                // Add the point to the set and remove it from the remainings
                remainings.Remove(farther);
                farthest.Add(farther);
            }
            return remainings;
        }

        /**
         * Find the first point for the FPS
         * 
         * The Furthest Point Sampling (FPS) need to select an initial point.
         * To do so we will take the furthest point from the average point of the point cloud.
         * 
         */
        private Vector3 getFirstPoint()
        {
            List<UnturnedUser> users = this.teamPlugin.storage.GetAllPlayers();
            // Calculate average point
            Vector3 average = users.Select(x => x.Player.Transform.Position)
                .Aggregate(Vector3.Zero, (acc, v) => Vector3.Add(acc, v)) / users.Count;
            Vector3 result = Vector3.Zero;
            float maxDitance = 0;
            users.Select(x => x.Player.Transform.Position).ForEach((Vector3 position) =>
            {
                float distance = Vector3.Distance(average, position);
                if (distance > maxDitance)
                {
                    maxDitance = distance;
                    result = position;
                }
            });
            return result;
        }
    }
}
